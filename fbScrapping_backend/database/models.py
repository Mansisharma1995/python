import enum
from operator import truediv
from config import db
from datetime import datetime
from sqlalchemy.dialects.postgresql import JSON

class Base(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)

    created_at = db.Column(db.DateTime, default=datetime.now)
    updated_at = db.Column(db.DateTime,
                           default=datetime.now,
                           onupdate=datetime.now)
    def __repr__(self):
        return '<{}: {}>'.format(self.__class__.__name__, getattr(self, 'id', 'None'))



class Influencer(Base):
	__tablename__ = 'influenecer'
	
	
	name= db.Column(db.String(250), nullable=False)
	username= db.Column(db.String(250), nullable=False)

class Admin(Base):
	__tablename__ = 'admin'
	
	name= db.Column(db.String(250), nullable=False)
	username= db.Column(db.String(250), nullable=False ,unique=True)
	psw= db.Column(db.String(250), nullable=False)
	
class User(Base):
	__tablename__ = 'user'
	
	name= db.Column(db.String(250), nullable=False)
	username= db.Column(db.String(250), nullable=False ,unique=True)
	psw= db.Column(db.String(250), nullable=False)



db.create_all()
db.session.commit()