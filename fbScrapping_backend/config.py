import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__,static_folder='static',template_folder= 'template')
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
database_path = APP_ROOT+'/fbScrapping.db'
print(APP_ROOT,'----------------------')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+database_path

db = SQLAlchemy(app)
BASE_URL = 'http://0.0.0.0:5000'

