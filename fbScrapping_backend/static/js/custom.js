$(document).ready(function() {
    $(".material-form-field input,.material-form-field select").on("blur", function() {
        if ($(this).val() != "") {
            $(this).siblings(".material-form-field-label").addClass("focused")
        } else {
            $(this).siblings(".material-form-field-label").removeClass("focused")
        }
    });
    if ($(".blink").length) {
        var i = 0;
        setInterval(function() {
            $(".blink").css("opacity", "0");
            $(".blink:eq(" + i + ")").css("opacity", "1");

            if (i == 1) {
                i = 0;
            } else {
                i++;
            }
        }, 2000);
    }
    $(window).on("load", function() {
        $(".material-form-field input,.material-form-field select").each(function() {
            if ($(this).val() != "") {
                $(this).siblings(".material-form-field-label").addClass("focused")
            } else {
                $(this).siblings(".material-form-field-label").removeClass("focused")
            }
        })
    });
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
    $(window).scroll(function() {
        var sticky = $('.sticky-footer'),
            scroll = $(window).scrollTop();

        if (scroll >= 100) sticky.addClass('fixed');
        else sticky.removeClass('fixed');
    });


});

$(document).ready(function() {
    $(".admin-menu").click(function() {
        $(".mobile-popup").show(500);
        $(".overlay").show();
    });

    $(".close").click(function() {
        $(".mobile-popup").hide(500);
        $(".overlay").hide();
    });

});


if (window.matchMedia("(max-width: 767px)").matches) {
    {
        $(document).ready(function() {

            $(".mobile-popup .nav-link").click(function() {
                $(".mobile-popup").hide(500);
                $(".overlay").hide();
            });
        });
    }
}