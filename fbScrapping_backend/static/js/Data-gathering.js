var lse ="";
var fullyear = new Date().getFullYear();
var API_URL ='http://127.0.0.1:5000';
//var API_URL ='http://122.162.242.226:5000';
//var API_URL ='http://192.168.1.7:5000';



$(document).ready(function() {
	
				$.ajax({
						url : API_URL+"/adminUserDetail",
						type : 'POST',
						dataType : 'json',
						success : function(data) {
							var obj = data['data'];
							var selOpts = "";
							for (var i = 0;i < obj.length;i++){
								var idn = obj[i]["id"];
								var n = obj[i]["name"];
								var e = obj[i]["username"];
								selOpts += "<option value='"+idn+"'>"+n+"-"+e+"</option>";
								}
							$('#userinfo').append(selOpts);
							$('#influencerinfo').append(selOpts);
							$('#locinfo').append(selOpts);
							$('#sublocinfo').append(selOpts);
							
							

						}
					});	
			
			if (typeof(Storage) !== "undefined") {
					
					// Retrieve
					 lse = document.getElementById("result").innerHTML = localStorage.getItem("unid");
					
					if (lse == null || lse == "" || lse == undefined)
					{
						
						$.ajax({
						url: API_URL+'/index',
						type: "POST",
						dataType: 'json',
						contentType: 'application/json',
						data: JSON.stringify('template'),
						success: function(response) {
							
							window.location.href = response.redirect
						}
				});
					
					}
				} else {
					document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
				}
				
			
			$.fn.dataTable.ext.errMode = 'none';
			$("#loader-gif").hide();
			$("#loc-gif").hide();
			
			$(window).scroll(function() {
				var sticky = $('.sticky-footer'),
					scroll = $(window).scrollTop();

				if (scroll >= 100) sticky.addClass('fixed');
				
				else sticky.removeClass('fixed');
			});
					
				document.getElementById("geo").click();
				
		});
		
		var arr =[] ;
		var hash_date=[];
		Date.prototype.addDays = function(days) {
			let d = new Date(this.valueOf());
			d.setDate(d.getDate() + days);
			var de = d.toLocaleDateString('en-us', { year:"numeric", month:"2-digit",day: "2-digit"});
			var de_date = de.split('/');
			
			var dr_date='';
			
				if (de_date[2] == fullyear){
					 dr_date= de_date[1]+" "+monConvertor(de_date[0])
					 modDate = de_date[1]+" "+monConvertor(de_date[0]).substring(0,3)
				}
				else
				{
					 dr_date= de_date[1]+" "+monConvertor(de_date[0])+" "+de_date[2]
					 modDate = de_date[1]+" "+monConvertor(de_date[0]).substring(0,3)+" "+de_date[2]
				}
			
			
			
			arr.push(dr_date);
			arr.push(modDate);
			
			return arr;
		}
		
		$("form#formdata").submit(function(e) {
			e.preventDefault();  
			$("#infucencer_data").DataTable().clear().destroy();
			
			var formData = new FormData(this);
			var ed = $("#exceldata").val();
			if ( ed != null && ed!="" && ed != undefined )
			{
				$("#load-gif").show();
				$.ajax({
					url: API_URL+"/upload_data",
					type: 'POST',
					data: formData,
					success: function (data) {
						$.ajax({
							url : API_URL+"/get_influencer",
							type : 'POST',
							dataType : 'json',
							success : function(data) {
							var obj = data['data'];
							infuencerData(obj);
							}
						});	
					  
					},
					cache: false,
					contentType: false,
					processData: false
				});
			}
			else
			{
				alert("First Select a File to Upload");
			}
	
		});

		function infuencerData(obj){
		
			//$("#load-gif").hide();
			$('#infucencer_data').dataTable({
			
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"bDestroy": true,
				"responsive": true,
				"paging":   true,
				data :obj,
				"columns": [
					{ "data": "name" },
					{ "data": "username" },
					{
						data: "id",
						className: "dt-center editor-delete",
						
						orderable: false
					}
				],
									
				"columnDefs": [{
					responsivePriority: 0,
					targets: 1,
					"render": function(data, type, full, meta) {
						return data ;
					}
					},
					{
					responsivePriority: 0,
					targets: 2,
					"render": function(data, type, full, meta) {
						
						var delRecord = '<img  height="20" width="20" src="../static/images/delete.svg" onclick ="infudeleteRecord('+data+');" alt="" />';
						
						//var delRecord = '<i class="fa fa-trash icon-large" height="100" width="100"  onclick ="deleteRecord('+data+');"/>';
						return delRecord ;
					}
					
					},
				]
			});
		
		};

		function infudeleteRecord(data) {
			var uid = data;
			if (confirm("Are you sure you want to delete this account?")) {
				$.ajax({
					  url : API_URL+"/deleteInfulencerProfile?uid="+uid,
					  type : 'POST',
					  dataType : 'json',
					  success : function(data) {
					  $.ajax({
							  url : API_URL+"/get_influencer",
							  type : 'POST',
							  dataType : 'json',
							  success : function(data) {
							  var obj = data['data'];
							  infuencerData(obj);
							  }
						  });	
						  
					  }
			  });	
				  
			  
			} else {
			  var txt = "You pressed Cancel!";
			}
		   
		  };
		  
		  
		  
		function openTab(id , cl){
			document.getElementById("tab1").style.display = "none";
			tabcontent = document.getElementsByClassName("tabcontent");
			$(".tablinks").css('background-color', 'transparent' );
			var tabcontent , i ;
			
				for (i = 0; i < tabcontent.length; i++) {
					tabcontent[i].style.display = "none";
				}
				
			document.getElementById(id).style.display = "block";
			document.getElementById(cl).style.backgroundColor = "transparent";
			 
			if (id == 'influencer' ){
				
				$.ajax({
					url : API_URL+"/get_influencer",
					type : 'POST',
					dataType : 'json',
					success : function(data) {
						var obj = data['data'];
						
						for (var i = 0;i < obj.length;i++){
							var e = obj[i]["username"];
							
						}
						infuencerData(obj);
					}
				});	
			}
			if (id == 'admin' ){
				
				$.ajax({
							url : API_URL+"/adminUserDetail",
							type : 'POST',
							dataType : 'json',
							success : function(data) {
							var obj = data['data'];
							userDetailData(obj);
							}
						});	
			}
			if (id == 'pm' ){
				
				$.ajax({
							url : API_URL+"/pmUserDetail",
							type : 'POST',
							dataType : 'json',
							success : function(data) {
							var obj = data['data'];
							pmuserDetailData(obj);
							}
						});	
			}
			if (id == 'location' ){
				
				document.getElementById("tab1").style.display = "block";
				
			}
		
		};
		
		function searchUser()
		{
			var uid =$("#influencerinfo option:selected" ).val();
			$("#loc").removeClass("col-md-3");
			$("#geo").removeClass("col-md-3");
			$("#loc").addClass("col-md-2");
			$("#geo").addClass("col-md-2");
			
			document.getElementById("influ").style.display = "block";
			document.getElementById("influ").click();
			$("#loading-gif").show();
			
			$.ajax({
				url : API_URL+"/get_influencer",
				type : 'POST',
				dataType : 'json',
				success : function(data) {
					var obj = data['data'];
					jsonObj = [];
					for (var i = 0;i < obj.length;i++){
						var e = obj[i]["username"];
						item = {}
						item ["username"] = e;
						jsonObj.push(item);

					}
					
					var pu = jsonObj;
					
					$.ajax({
						url : API_URL+"/infulencerDetails?uid="+uid+"&pu="+pu,
						type : 'POST',
						dataType : 'json',
						success : function(data) {
							$("#loading-gif").hide();
							console.log(data);
							var obj = data
							createPost(obj);
						}
					});	
				}
			});	
		};
	
		
		function showTable(){
		
			var p =$("#ph").val();
			var d = $("#optionData").val();
			var hd = $("#hdate").val();
			var ed = $("#edate").val();
			var uid =$("#userinfo option:selected" ).val();
			var datarange = $("#dataRange option:selected").val();
			var date1 = new Date(hd);
			var date2 = new Date(ed);
			// To calculate the time difference of two dates
			var Difference_In_Time = date2.getTime() - date1.getTime();
  
			// To calculate the no. of days between two dates
			var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
			//alert(Difference_In_Days);
			
			var dr ;
			if (datarange == 1)
			{
				dr=1500;
			}
			else if (datarange == 2)
			{
				dr=4000;
			}
			else if (datarange == 3)
			{
				dr=8000;
			}
			
			//var hash_date="";
			
			
			document.getElementById("cntdata").style.display = "none";
			if ( hd != null && hd!="" && hd != undefined )
			{
				if (ed != null && ed!="" && ed != undefined)
				{
					//alert("hd and ed both are not null "+hd+"ed values"+ed);
					for (var i = 0; i <= Difference_In_Days;i++){
						
						date1.addDays(i);
					};
					
					hash_date = arr;
					
				}
				else
				{
					//alert("hd is not null but ed is null "+hd+"ed values"+ed);
					var hd_date = hd.split('-');
					if (hd_date[0] == fullyear){
						hs_date= hd_date[2]+" "+monConvertor(hd_date[1])
						modDate = hd_date[2]+" "+monConvertor(hd_date[1]).substring(0,3)
					}
					else
					{
					 hs_date= hd_date[2]+" "+monConvertor(hd_date[1])+" "+hd_date[0]
					 modDate = hd_date[2]+" "+monConvertor(hd_date[1]).substring(0,3)+" "+hd_date[0]
					}
					hash_date.push(hs_date);
					hash_date.push(modDate);
					
				}
			}
			else
			{
				hash_date=[];
				//alert("hd and ed both are null "+hd+"ed values"+ed);
				
			}
			
			
			if (p != null && p != "")
			{
				if (d == "1")
				{
					if (p.indexOf(' ') >= 0){
						alert('Hashtag should not contain spaces');
					}
				
					else{
					$("#admin-order").DataTable().clear().destroy();
					$('.filters').remove();
					$("#loader-gif").show();
					sTable(p,hash_date,dr);
					}
				}
				else if (d == "2")
				{
					if (uid == "Choose Profile")
						{
							alert("Select Profile First")
						} 
					else
					{	
						$("#admin-order").DataTable().clear().destroy();
						$('.filters').remove();
						$("#loader-gif").show();
						dTable(p,uid,dr,hash_date);
					}
					
				}
			}
			else 
			{
				alert('Please Enter Phrase/Hashtag');
			}
		};
			
		function sTable(p,h,dr){

			$.ajax({
					url : API_URL+"/get_data?s="+p+"&h="+h+"&dr="+dr,
					type : 'POST',
					dataType : 'json',
					success : function(data) {
						$("#loader-gif").hide();
						
						$('#admin-order thead tr')
						.clone(true)
						.addClass('filters')
						.appendTo('#admin-order thead');
						
						var obj = data['pData']
						/*var ls = obj.length - 1;
						var ctdata = obj[ls]["cnt"];*/
						var ctdata = data['cnt'];
						var htag = $("#ph").val();
						var hd = $("#hdate").val();
						var ed = $("#edate").val();
						if (hd != null && hd!="" && hd != undefined)
						{
							var hd_date = hd.split('-');
							if (hd_date[0] == fullyear){
								var hash_date= hd_date[2]+" "+monConvertor(hd_date[1])
							}
							else
							{
								var hash_date= hd_date[2]+" "+monConvertor(hd_date[1])+" "+hd_date[0]
							}
							document.getElementById("cntdata").style.display = "block";
							if (ed != null && ed!="" && ed != undefined)
							{
								var ed_date = ed.split('-');
								if (ed_date[0] == fullyear){
									var edhash_date= ed_date[2]+" "+monConvertor(ed_date[1])
								}
								else
								{
									var edhash_date= ed_date[2]+" "+monConvertor(ed_date[1])+" "+ed_date[0]
								}
								
								$("#cntdt").val(ctdata+' post found for '+htag+' on date range of '+hash_date+" to "+edhash_date);
							
							}
							else{
								
								$("#cntdt").val(ctdata+' post found for '+htag+' on '+hash_date);
							}
						}
						$('#admin-order tr td ').each(function() {
								if ($(this).find('td:empty').length) $(this).remove();
							});
						createTable(obj)
					}
			});	
		};
		
		function dTable(p,uid,dr,h){

		
			$.ajax({
				url : API_URL+"/get_phrase_data?p="+p+"&uid="+uid+"&dr="+dr+"&h="+h,
				type : 'POST',
				dataType : 'json',
				success : function(data) {
					$("#loader-gif").hide();
					$('#admin-order thead tr')
					.clone(true)
					.addClass('filters')
					.appendTo('#admin-order thead');
					var obj = data
					createTable(obj)
				}
			});	
		};
			
		function createPost(obj){
		
			var count = 0;
			$('#infucencer_post').show().dataTable({
				"bDestroy": true,
				initComplete: function () {
					var api = this.api();

					// For each column
					api
					.columns()
					.eq(0)
					.each(function (colIdx) {
						// Set the header cell to contain the input element
						var cell = $('.filters th').eq(
							
							$(api.column(colIdx).header()).index()
						);
						
						var title = $(cell).text();
						if (title != 'Image'){
							$(cell).html('<input type="text" placeholder="' + title + '" />');
						}
						else
						{
							$(cell).html('<input type="hidden" />');
						}
						// On every keypress in this input
						$(
							'input',
							$('.filters th').eq($(api.column(colIdx).header()).index())
						)
						.off('keyup change')
						.on('keyup change', function (e) {
							e.stopPropagation();

							// Get the search value
							$(this).attr('title', $(this).val());
							var regexr = '({search})'; //$(this).parents('th').find('select').val();

							var cursorPosition = this.selectionStart;
							// Search the column for that value
							api
								.column(colIdx)
								.search(
									this.value != ''
										? regexr.replace('{search}', '(((' + this.value + ')))')
										: '',
									this.value != '',
									this.value == ''
								)
								.draw();

							$(this)
								.focus()[0]
								.setSelectionRange(cursorPosition, cursorPosition);
						});
					});
				},
				
				data :obj,
				"responsive": true,
				"columns": [
					{ "data": "postProfile" },
					{ "data": "post_data" },
					{ "data": "post_time" },
					{ "data": "post_image" }
				],
						
				"columnDefs": [{
					responsivePriority: 0,
					targets: 3,
					"render": function(data, type, full, meta) {
							
						var imgElement = "";
						if (data != null && data!="" && data != undefined)
						{
							for (var n=0; n < data.length;n++){
								if (n == 0){
									var d = data[n];
									count = count+1;
									imgElement = '<img id="imgtab'+count+'" height="50" width="50" onclick ="imgClick(this);" class="something" src="'+data[n]+'">';	
									
								}
								else
								{
									count = count+1;
									imgElement = imgElement + '<img id ="imgtab'+count+'" height="50" width="50" onclick ="imgClick(this);" class="something" src="'+data[n]+'">';
									
								}
							}
						
						}
						
						return imgElement ;
					}
				},
				
				{
					responsivePriority: 0,
					targets: 1,
					"render": function(data, type, full, meta) {
						return data ;
					}
				},
				]
			});
		};
			
		function createTable(obj){
			var count = 0;
			$('#admin-order').show().dataTable({
				"bDestroy": true,
				
				initComplete: function () {
					var api = this.api();
					// For each column
					api
					.columns()
					.eq(0)
					.each(function (colIdx) {
						// Set the header cell to contain the input element
						var cell = $('.filters th').eq(
							
							$(api.column(colIdx).header()).index()
						);
						
						var title = $(cell).text();
						if (title != 'Image'){
							$(cell).html('<input type="text" placeholder="' + title + '" />');
						}
						else
						{
							$(cell).html('<input type="hidden" />');
						}
						// On every keypress in this input
						$(
							'input',
							$('.filters th').eq($(api.column(colIdx).header()).index())
						)
							.off('keyup change')
						.on('keyup change', function (e) {
							e.stopPropagation();

							// Get the search value
							$(this).attr('title', $(this).val());
							var regexr = '({search})'; //$(this).parents('th').find('select').val();

							var cursorPosition = this.selectionStart;
							// Search the column for that value
							api
								.column(colIdx)
								.search(
									this.value != ''
										? regexr.replace('{search}', '(((' + this.value + ')))')
										: '',
									this.value != '',
									this.value == ''
								)
								.draw();

							$(this)
								.focus()[0]
								.setSelectionRange(cursorPosition, cursorPosition);
						});
					});
				},
				dom: 'Bfrtip',
				buttons: [
				'excel'
					],
				data :obj,
				"responsive": true,
				
				
				"columns": [
					{ "data": "postProfile" },
					{ "data": "post_data" },
					{ "data": "post_time" },
					{ "data": "post_image" }
				],
				hideEmptyCols: true,
						
				"columnDefs": [{
					responsivePriority: 0,
					targets: 3,
					"render": function(data, type, full, meta) {
						
						var imgElement = "";
						if (data != null && data!="" && data != undefined)
						{
							for (var n=0; n < data.length;n++){
								if (n == 0){
									var d = data[n];
									count = count+1;
									imgElement = '<img id="imgtab'+count+'" height="50" width="50" onclick ="imgClick(this);" class="something" src="'+data[n]+'">';	
									
								}
								else
								{
									count = count+1;
									imgElement = imgElement + '<img id ="imgtab'+count+'" height="50" width="50" onclick ="imgClick(this);" class="something" src="'+data[n]+'">';
									
								}
								
							}
						
						}
						
						return imgElement ;
					}
					},
				
					{
						responsivePriority: 0,
						targets: 1,
						"render": function(data, type, full, meta) {
							return data ;
						}
					
					},

				]
			});
		
		
		};
		
		function imgClick(d)
		{
				
			var atr = d.getAttribute('id');
			var src = d.getAttribute('src');
			
			// Get the modal
			var modal = document.getElementById("myModal");
			
			// Get the image and insert it inside the modal
			var img = document.getElementById(atr);
			var modalImg = document.getElementById("img01");
			modal.style.display = "block";
			modalImg.src = src;
				
			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("close")[0];

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() { 
				modal.style.display = "none";
			}
		};
		
		$("#optionData").change(function() {
			var od = $("#optionData").val();
			$("#ph").val('');
				
			if (od == "2")
			{
				
				document.getElementById("userinf").style.display = "block";
				//document.getElementById("hdtdata").style.display = "none";
				//document.getElementById("cntdata").style.display = "none";
				//document.getElementById("enddate").style.display = "none";
				//document.getElementById("edate").style.display = "none";
			}
			else if (od == "1")
			{
				document.getElementById("userinf").style.display = "none";
				document.getElementById("hdtdata").style.display = "block";
				document.getElementById("enddate").style.display = "block";
			}
		});
		
		function monConvertor(m){
			if(m=="01")
				{
					return "January";
				}
			else if (m=="02")
				{
					return "February";
				}
			else if (m=="03")
				{
					return "March";
				}
			else if (m=="04")
				{
					return "April";
				}
			else if (m=="05")
				{
					return "May";
				}
			else if (m=="06")
				{
					return "June";
				}
			else if (m=="07")
				{
					return "July";
				}
			else if (m=="08")
				{
					return "August";
				}
			else if (m=="09")
				{
					return "September";
				}
			else if (m=="10")
				{
					return "October";
				}
			else if (m=="11")
				{
					return "November";
				}
			else if (m=="12")
				{
					return "December";
				}
		};
		
		
		$("#addnew").click(function() {
				
				$("#name").val("");
				$("#username").val("");
				$("#psw").val("");
				$("#cfirmpsw").val("");
				$('#userids').modal('show');
		});
		
		$("form#userdetail").submit(function(e) {
			e.preventDefault();
			var psw = $('#psw').val();
			var cfirmpsw = $('#cfirmpsw').val();
			var usrname = $('#username').val();
			if (usrname.indexOf(' ') <= 0)
			{
				if (psw  == cfirmpsw)
			{
			$('#userids').modal('hide');
			//$("#infucencer_data").DataTable().clear().destroy();
			var formData = new FormData(this);
			
				$.ajax({
					url: API_URL+"/postAdminDetail",
					type: 'POST',
					data: formData,
					success: function (data) {
						alert(data['message'])
						
						$.ajax({
							url : API_URL+"/adminUserDetail",
							type : 'POST',
							dataType : 'json',
							success : function(data) {
							var obj = data['data'];
							userDetailData(obj);
							}
						});	
					  
					},
					error: function (data) {
					alert(data)
					},
					cache: false,
					contentType: false,
					processData: false
				});
			}
			else 
			{
				alert("Password not Matched");
			}
			}
			else{
			
				alert("Username does not Contain Spaces");
			
			}
	
		});
		
		function userDetailData(obj){
		
			
			$('#user_data').dataTable({
			
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"bDestroy": true,
				"responsive": true,
				"paging":   true,
				data :obj,
				"columns": [
					{ "data": "name" },
					{ "data": "username" },
					{
						data: "id",
						className: "dt-center editor-delete",
						
						orderable: false
					}
				],
									
				"columnDefs": [{
					responsivePriority: 0,
					targets: 1,
					"render": function(data, type, full, meta) {
						return data ;
					}
					},
					{
					responsivePriority: 0,
					targets: 2,
					"render": function(data, type, full, meta) {
						
						var delRecord = '<img  height="20" width="20" src="../static/images/delete.svg" onclick ="deleteRecord('+data+');" alt="" />';
						
						//var delRecord = '<i class="fa fa-trash icon-large" height="100" width="100"  onclick ="deleteRecord('+data+');"/>';
						return delRecord ;
					}
					
					},
				]
			});
		
		};
		
		function showLocation(){
		
			//var p =$("#ploc").val();
			var l =$("#loct").val();
			var uid =$( "#locinfo option:selected" ).val();
			var datarange = $("#dataRangeloc option:selected").val();
			var dr ;
			if (datarange == 1)
			{
				dr=8000;
			}
			else if (datarange == 2)
			{
				dr=12000;
			}
			else if (datarange == 3)
			{
				dr=15000;
			}
			document.getElementById("geo_loc-order").style.display = "none";
			
			$("#hashtagrecord > tbody").empty();
			$("#geo_loc-order").DataTable().clear().destroy();
				if (l != null && l != "")
				{
					
						$("#admin-order").DataTable().clear().destroy();
						$('.filters').remove();
						$("#loc-gif").show();
						lTable(uid,l,dr);
					
				}
				else
				{
					alert('Please Enter Phrase/Hashtag');
				}
			
		};
		
		function showSubLocation(){
		
			var p =$("#ploc").val();
			var l =$("#subloct").val();
			var uid =$( "#sublocinfo option:selected" ).val();
			var datarange = $("#subdataRangeloc option:selected").val();
			var dr ;
			if (datarange == 1)
			{
				dr=8000;
			}
			else if (datarange == 2)
			{
				dr=12000;
			}
			else if (datarange == 3)
			{
				dr=15000;
			}
				if (p != null && p != "")
				{
					if (l != null && l != "")
					{
						
							$("#subloc-gif").show();
							sublTable(uid,p,l,dr);
						
					}
					else
					{
						alert('Please Enter location');
					}
				}
		
				else
				{
					alert('Please Enter Phrase');
				}
			
		};
		var objdata="";
		function lTable(uid,l,dr){
			
			$.ajax({
					url : API_URL+"/geo_location_data?uid="+uid+"&l="+l+"&dr="+dr,
					type : 'POST',
					dataType : 'json',
					success : function(data) {
						$("#loc-gif").hide();
						objdata = data['pList']
						var hashdata = data['hash']
						//alert('length'+hashdata.length)
						//dict = {0:{1:'a'}, 1:{2:'b'}, 2:{3:'c'}}
						var count =0;
							for (var key in hashdata){
							console.log( key, hashdata[key] );
							keypublic = key;
							var record = key
							console.log('data'+record)
							var result = record.slice(1);
							console.log('result'+result)
							if (result.length > 1)
							{
							var tr  ="<tr>"+                                               
							"<td style='font-weight:bold' id ="+count+">"+result+"</td>"+                                              
							"<td style='font-weight:bold'onclick=\"updateRow("+count+");\"><a href = \"#\">"+hashdata[key]+"</a></td>"+ 
							"</tr>";
							}
							count = count+1;
							$("#hashtagrecord").append(tr);
								}
							 var table = $('#hashtagrecord').DataTable( {
								lengthMenu: [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
								paging:         true,
								 "order": [[ 1, "desc" ]]
								
							} );
					
					}
			});	
		};
		
		function sublTable(uid,p,l,dr){
			
			$.ajax({
					url : API_URL+"/sub_geo_location_data?uid="+uid+"&p="+p+"&l="+l+"&dr="+dr,
					type : 'POST',
					dataType : 'json',
					success : function(data) {
						$("#subloc-gif").hide();
						obj = data
						subLocTable(obj);
					
					}
			});	
		};
		function locTable(obj){
			var count = 0;
			var geoloctable = $('#geo_loc-order').show().dataTable({
				"bDestroy": true,
				dom: 'Bfrtip',
				buttons: [
				'excel'
					],
				data :obj,
				"responsive": true,
				"search": "Search:",
				"columns": [
					{ "data": "postProfile" },
					{ "data": "post_data" },
					{ "data": "post_time" },
					{ "data": "post_image" }
				],
				
				"initComplete": function () {
					var hidval = $("#hidnval").val();
					this.api().search(hidval).draw();
					},
						
				"columnDefs": [{
					responsivePriority: 0,
					targets: 3,
					"render": function(data, type, full, meta) {
						
						var imgElement = "";
						if (data != null && data!="" && data != undefined)
						{
							for (var n=0; n < data.length;n++){
								if (n == 0){
									var d = data[n];
									count = count+1;
									imgElement = '<img id="imgtab'+count+'" height="50" width="50" onclick ="imgClick(this);" class="something" src="'+data[n]+'">';	
									
								}
								else
								{
									count = count+1;
									imgElement = imgElement + '<img id ="imgtab'+count+'" height="50" width="50" onclick ="imgClick(this);" class="something" src="'+data[n]+'">';
									
								}
								
							}
						
						}
						
						return imgElement ;
					}
					},
				
					{
						responsivePriority: 0,
						targets: 1,
						"render": function(data, type, full, meta) {
							return data ;
						}
					
					},

				]
			});
		
		
		};
		
		function subLocTable(obj){
			var count = 0;
			$('#sub_geo_loc-order').show().dataTable({
				"bDestroy": true,
				dom: 'Bfrtip',
				buttons: [
				'excel'
					],
				data :obj,
				"responsive": true,
				
				"columns": [
					{ "data": "postProfile" },
					{ "data": "post_data" },
					{ "data": "post_time" },
					{ "data": "post_image" }
				],
				
				"columnDefs": [{
					responsivePriority: 0,
					targets: 3,
					"render": function(data, type, full, meta) {
						
						var imgElement = "";
						if (data != null && data!="" && data != undefined)
						{
							for (var n=0; n < data.length;n++){
								if (n == 0){
									var d = data[n];
									count = count+1;
									imgElement = '<img id="imgtab'+count+'" height="50" width="50" onclick ="imgClick(this);" class="something" src="'+data[n]+'">';	
									
								}
								else
								{
									count = count+1;
									imgElement = imgElement + '<img id ="imgtab'+count+'" height="50" width="50" onclick ="imgClick(this);" class="something" src="'+data[n]+'">';
									
								}
								
							}
						
						}
						
						return imgElement ;
					}
					},
				
					{
						responsivePriority: 0,
						targets: 1,
						"render": function(data, type, full, meta) {
							return data ;
						}
					
					},

				]
			});
		
		
		};
		
		
		function deleteRecord(data) {
		  var uid = data;
		  if (confirm("Are you sure you want to delete this account?")) {
			  $.ajax({
					url : API_URL+"/deleteAdminProfile?uid="+uid,
					type : 'POST',
					dataType : 'json',
					success : function(data) {
					$.ajax({
							url : API_URL+"/adminUserDetail",
							type : 'POST',
							dataType : 'json',
							success : function(data) {
							var obj = data['data'];
							userDetailData(obj);
							}
						});	
						
					}
			});	
				
			
		  } else {
			var txt = "You pressed Cancel!";
		  }
		 
		};
		
		function updateRow(value)
		{
			$("#hidnval").val($("#"+value).html());
			
			//document.getElementById("geo_loc-order").style.display = "block";
			locTable(objdata);
		};
		
		$("#addnewpm").click(function() {
				
				$("#pmname").val("");
				$("#pmusername").val("");
				$("#pmpsw").val("");
				$("#pmcfirmpsw").val("");
				$('#pmids').modal('show');
		});
		
		$("form#pmdetail").submit(function(e) {
			e.preventDefault();
			var psw = $('#pmpsw').val();
			var cfirmpsw = $('#pmcfirmpsw').val();
			var usrname = $('#pmusername').val();
			if (usrname.indexOf(' ') <= 0)
			{
				if (psw  == cfirmpsw)
			{
			$('#pmids').modal('hide');
			//$("#infucencer_data").DataTable().clear().destroy();
			var formData = new FormData(this);
			
				$.ajax({
					url: API_URL+"/upload_pmuser_detail",
					type: 'POST',
					data: formData,
					success: function (data) {
						alert(data['message'])
						
						$.ajax({
							url : API_URL+"/pmUserDetail",
							type : 'POST',
							dataType : 'json',
							success : function(data) {
							var obj = data['data'];
							pmuserDetailData(obj);
							}
						});	
					  
					},
					error: function (data) {
					alert(data)
					},
					cache: false,
					contentType: false,
					processData: false
				});
			}
			else 
			{
				alert("Password not Matched");
			}
			}
			else{
			
				alert("Username does not Contain Spaces");
			
			}
	
		});
		
		function pmuserDetailData(obj){
		
			
			$('#user_datapm').dataTable({
			
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"bDestroy": true,
				"responsive": true,
				"paging":   true,
				data :obj,
				"columns": [
					{"data": "name" },
					{ "data": "username" },
					{
						data: "id",
						className: "dt-center editor-delete",
						
						orderable: false
					}
				],
									
				"columnDefs": [{
					responsivePriority: 0,
					targets: 1,
					"render": function(data, type, full, meta) {
						return data ;
					}
					},
					{
					responsivePriority: 0,
					targets: 2,
					"render": function(data, type, full, meta) {
						
						var delRecord = '<img  height="20" width="20" src="../static/images/delete.svg" onclick ="pmdeleteRecord('+data+');" alt="" />';
						
						//var delRecord = '<i class="fa fa-trash icon-large" height="100" width="100"  onclick ="deleteRecord('+data+');"/>';
						return delRecord ;
					}
					
					},
				]
			});
		
		};
		
		function pmdeleteRecord(data) {
		  var uid = data;
		  if (confirm("Are you sure you want to delete this account?")) {
			  $.ajax({
					url : API_URL+"/deletePmProfile?uid="+uid,
					type : 'POST',
					dataType : 'json',
					success : function(data) {
					$.ajax({
							url : API_URL+"/pmUserDetail",
							type : 'POST',
							dataType : 'json',
							success : function(data) {
							var obj = data['data'];
							pmuserDetailData(obj);
							}
						});	
						
					}
			});	
				
			
		  } else {
			var txt = "You pressed Cancel!";
		  }
		 
		};
		
// Login page authenciation
		

	$("form#loginform").submit(function(e) {
		
			e.preventDefault();
			var psw = $('#password').val();
			var usrname = $('#username').val();
			
			var formData = new FormData(this);
			
				$.ajax({
					url: API_URL+"/authenciate_detail?uid="+usrname+"&psv="+psw,
					type: 'POST',
					data: formData,
					success: function (data) {
						var unid =data['data']
						if (typeof(Storage) !== "undefined") {
							// Store
							localStorage.setItem("unid", unid);
							;
						}
						else {
							document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
						}
						console.log(unid);
							
						if (data['message']== "Record Match"){

							$.ajax({
								url: API_URL+'/page',
								type: "POST",
								dataType: 'json',
								contentType: 'application/json',
								data: JSON.stringify('template'),
								success: function(response) {
									window.location.href = response.redirect
								}
							});
						}
						else{
							alert("Icorrect Username or Password")
						}
					},
					error: function (data) {
							alert("Icorrect Username or Password")
					},
					cache: false,
					contentType: false,
					processData: false,
					
				});		
	});
		
		function signOut()
		{
			
			if (typeof(Storage) !== "undefined") {
					
					// Retrieve
					 localStorage.clear();
			}

				$.ajax({
						url: API_URL+'/index',
						type: "POST",
						dataType: 'json',
						contentType: 'application/json',
						data: JSON.stringify('template'),
						success: function(response) {
							
							window.location.href = response.redirect
						}
				});
					
		};
		
		
		
			
		